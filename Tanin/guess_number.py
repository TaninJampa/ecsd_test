"""System module."""
import random


def guess_int(start, stop):
    """Function Guess int."""
    return random.randint(start, stop)


def guess_float(start, stop):
    """Function Guess float."""
    return random.uniform(start, stop)
