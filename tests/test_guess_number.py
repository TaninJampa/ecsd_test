from Tanin.guess_number import guess_int, guess_float
import unittest
import unittest.mock as mock


class TestGuessNumber(unittest.TestCase):

    # stub assign expected value
    # random.randint
    def test_get_random_randint_1(self):
        with mock.patch('random.randint', mock.MagicMock()) as mock_randint:
            # expect to method to return 1
            mock_randint.return_value = 1
            result = guess_int(1, 10)
            self.assertEqual(result, 1)
            mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=2)
    def test_get_random_randint_2(self, mock_randint):
        # expect to method to return 2
        mock_randint.guess_int.return_value = 2
        result = guess_int(1, 10)
        self.assertEqual(result, 2)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=3)
    def test_get_random_randint_3(self, mock_randint):
        # expect to method to return 3
        mock_randint.guess_int.return_value = 1
        result = guess_int(1, 10)
        self.assertEqual(result, 3)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=4)
    def test_get_random_randint_4(self, mock_randint):
        # expect to method to return 4
        mock_randint.guess_int.return_value = 4
        result = guess_int(1, 10)
        self.assertEqual(result, 4)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=5)
    def test_get_random_randint_5(self, mock_randint):
        # expect to method to return 5
        mock_randint.guess_int.return_value = 5
        result = guess_int(1, 10)
        self.assertEqual(result, 5)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=6)
    def test_get_random_randint_6(self, mock_randint):
        # expect to method to return 6
        mock_randint.guess_int.return_value = 6
        result = guess_int(1, 10)
        self.assertEqual(result, 6)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=7)
    def test_get_random_randint_7(self, mock_randint):
        # expect to method to return 8
        mock_randint.guess_int.return_value = 7
        result = guess_int(1, 10)
        self.assertEqual(result, 7)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=8)
    def test_get_random_randint_8(self, mock_randint):
        # expect to method to return 8
        mock_randint.guess_int.return_value = 8
        result = guess_int(1, 10)
        self.assertEqual(result, 8)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=18)
    def test_get_random_randint_18(self, mock_randint):
        # expect to method to return 18
        mock_randint.guess_int.return_value = 18
        result = guess_int(1, 100)
        self.assertEqual(result, 18)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=120)
    def test_get_random_randint_120(self, mock_randint):
        # expect to method to return 8
        mock_randint.guess_int.return_value = 120
        result = guess_int(1, 150)
        self.assertEqual(result, 120)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=130)
    def test_get_random_randint_130(self, mock_randint):
        # expect to method to return 130
        mock_randint.guess_int.return_value = 130
        result = guess_int(1, 150)
        self.assertEqual(result, 130)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=150)
    def test_get_random_randint_150(self, mock_randint):
        # expect to method to return 150
        mock_randint.guess_int.return_value = 150
        result = guess_int(1, 150)
        self.assertEqual(result, 150)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=93)
    def test_get_random_randint_93(self, mock_randint):
        # expect to method to return 93
        mock_randint.guess_int.return_value = 93
        result = guess_int(1, 100)
        self.assertEqual(result, 93)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=57)
    def test_get_random_randint_57(self, mock_randint):
        # expect to method to return 57
        mock_randint.guess_int.return_value = 57
        result = guess_int(1, 100)
        self.assertEqual(result, 57)
        mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=111)
    def test_get_random_randint_111(self, mock_randint):
        # expect to method to return 111
        mock_randint.guess_int.return_value = 111
        result = guess_int(1, 150)
        self.assertEqual(result, 111)
        mock_randint.assert_called_once()

    # random.uniform
    @mock.patch('random.uniform', return_value=1.5)
    def test_get_random_uniform_1_5(self, mock_uniform):
        # expect to method to return 1.5
        mock_uniform.guess_float.return_value = 1.5
        result = guess_float(1.0, 10.0)
        self.assertEqual(result, 1.5)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=1.7)
    def test_get_random_uniform_1_7(self, mock_uniform):
        # expect to method to return 1.7
        mock_uniform.guess_float.return_value = 1.7
        result = guess_float(1.0, 10.0)
        self.assertEqual(result, 1.7)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=2.5)
    def test_get_random_uniform_2_5(self, mock_uniform):
        # expect to method to return 2.5
        mock_uniform.guess_float.return_value = 2.5
        result = guess_float(1.0, 10.0)
        self.assertEqual(result, 2.5)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=5.5)
    def test_get_random_uniform_5_5(self, mock_uniform):
        # expect to method to return 5.5
        mock_uniform.guess_float.return_value = 5.5
        result = guess_float(1.0, 10.0)
        self.assertEqual(result, 5.5)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=0.5)
    def test_get_random_uniform_0_5(self, mock_uniform):
        # expect to method to return 0.5
        mock_uniform.guess_float.return_value = 0.5
        result = guess_float(0.0, 10.0)
        self.assertEqual(result, 0.5)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=4.8)
    def test_get_random_uniform_4_8(self, mock_uniform):
        # expect to method to return 4.8
        mock_uniform.guess_float.return_value = 4.8
        result = guess_float(0.0, 10.0)
        self.assertEqual(result, 4.8)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=120.02)
    def test_get_random_uniform_1_2_0_0_2(self, mock_uniform):
        # expect to method to return 120.02
        mock_uniform.guess_float.return_value = 120.02
        result = guess_float(0.0, 210.0)
        self.assertEqual(result, 120.02)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=0.15)
    def test_get_random_uniform_0_1_5(self, mock_uniform):
        # expect to method to return 0.15
        mock_uniform.guess_float.return_value = 0.15
        result = guess_float(0.0, 10.0)
        self.assertEqual(result, 0.15)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=1.93)
    def test_get_random_uniform_1_9_3(self, mock_uniform):
        # expect to method to return 1.93
        mock_uniform.guess_float.return_value = 1.93
        result = guess_float(0.0, 10.0)
        self.assertEqual(result, 1.93)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=9.31)
    def test_get_random_uniform_9_3_1(self, mock_uniform):
        # expect to method to return 9.31
        mock_uniform.guess_float.return_value = 0.15
        result = guess_float(0.0, 10.0)
        self.assertEqual(result, 9.31)
        mock_uniform.assert_called_once()

    @mock.patch('random.uniform', return_value=3.19)
    def test_get_random_uniform_3_1_9(self, mock_uniform):
        # expect to method to return 3.19
        mock_uniform.guess_float.return_value = 0.15
        result = guess_float(0.0, 10.0)
        self.assertEqual(result, 3.19)
        mock_uniform.assert_called_once()


if __name__ == '__main__':
    unittest.main()
